from django.test import TestCase
from rest_framework import status
from model_bakery import baker
from django.contrib.auth.models import User
from rest_framework.test import APIClient

from category.models import Category
from product.models import Product
from utils.string_formatter import remove_tooman_from_string_and_return_price_number


def authenticate_user(api_client, user):
    return api_client.force_authenticate(user=user)


def create_or_update_product(api_client, sample_product_create_body):
    return api_client.post('/product/create-or-update/', sample_product_create_body, format='json')


def get_new_sample_product_create_body():
    return {
            "page_url": "http://digikala.com/DKP-1213",
            "shop_domain": "digikala.com",
            "name": "موبایل آیفون xs max 64Gb",
            "price": 1000,
            "is_available": True,
            "features": {
                "حافظه": "24 Gb"
              }
        }


class ProductTestCase(TestCase):

    def setUp(self):
        # create a sample tree of categories
        Category.objects.create(name='sth', id=1)
        Category.objects.create(name='sth', parent_id=1, id=2)
        Category.objects.create(name='sth', parent_id=1, id=3)
        Category.objects.create(name='sth', parent_id=2, id=4)
        Category.objects.create(name='sth', parent_id=3, id=5)
        Category.objects.create(name='sth', parent_id=3, id=6)

        self.admin_user = baker.make(User, is_staff=True)
        self.user = baker.make(User, is_staff=False)
        self.api_client = APIClient()
        self.sample_product_create_body = {
            "page_url": "http://digikala.com/DKP-1213",
            "shop_domain": "digikala.com",
            "name": "موبایل آیفون xs max 64Gb",
            "price": 1000,
            "is_available": True,
            "features": {
                "حافظه": "24 Gb"
              }
        }

    def test_if_leaf_category_filter_returns_related_products(self):
        product1 = baker.make(Product, category_id=4, uid='uTrwkuyVf5uc')
        product1.save()

        product2 = baker.make(Product, category_id=5, uid='kFrsouyRf5lp')
        product2.save()

        response = self.api_client.get("/product/list/", {'category_id': 4}, format='json').json()

        self.assertEqual(len(response['results']), 1)
        self.assertEqual(response['results'][0]['uid'], product1.uid)

    def test_if_parent_category_filter_returns_all_products_in_selected_child_categories(self):
        # category ids hierarchy:
        #   1 root
        #       2
        #           4
        #       3
        #           5
        #           6
        product1 = baker.make(Product, category_id=4, uid='uTrwkuyVf5uc')
        product1.save()
        product2 = baker.make(Product, category_id=5, uid='kFrsouyRf5lp')
        product2.save()
        product3 = baker.make(Product, category_id=6, uid='lPrwkuyVf0uc')
        product3.save()

        response = self.api_client.get("/product/list/", {'category_id': 3}, format='json').json()

        self.assertEqual(len(response['results']), 2)

        response = self.api_client.get("/product/list/", {'category_id': 2}, format='json').json()

        self.assertEqual(len(response['results']), 1)

        response = self.api_client.get("/product/list/", {'category_id': 1}, format='json').json()

        self.assertEqual(len(response['results']), 3)

    def test_if_non_admin_user_add_or_update_product_result_403_forbidden(self):

        response = self.api_client.post('/product/create-or-update/', get_new_sample_product_create_body(), format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_if_admin_user_can_successfully_add_or_update_products(self):

        authenticate_user(self.api_client, self.admin_user)
        response = self.api_client.post('/product/create-or-update/', get_new_sample_product_create_body(), format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual('uid' in response.json(), True)

    def test_if_product_price_change_is_saved_properly(self):

        authenticate_user(self.api_client, self.admin_user)

        # create product
        response = create_or_update_product(self.api_client, self.sample_product_create_body).json()
        product_uid = response['uid']

        # expected old and new prices
        old_price = self.sample_product_create_body['price']
        new_price = 2000
        self.sample_product_create_body['price'] = new_price

        # update product
        response = create_or_update_product(self.api_client, self.sample_product_create_body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # get changes list
        response = self.api_client.get("/product/price-change/list/", {'uid': product_uid}, format='json').json()
        product_changes_list = response['results']
        self.assertEqual\
            (old_price, remove_tooman_from_string_and_return_price_number(product_changes_list[0]['old_price']))
        self.assertEqual\
            (new_price, remove_tooman_from_string_and_return_price_number(product_changes_list[0]['new_price']))

    def test_if_product_availability_change_is_saved_properly(self):

        authenticate_user(self.api_client, self.admin_user)

        # create product
        response = create_or_update_product(self.api_client, self.sample_product_create_body).json()
        product_uid = response['uid']

        # expected old and new prices
        old_availability = self.sample_product_create_body['is_available']
        new_availability = False
        self.sample_product_create_body['is_available'] = new_availability

        # update product
        response = create_or_update_product(self.api_client, self.sample_product_create_body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # get changes list
        response = self.api_client.get("/product/price-change/list/", {'uid': product_uid}, format='json').json()
        product_changes_list = response['results']
        self.assertEqual(old_availability,product_changes_list[0]['old_availability'])
        self.assertEqual(new_availability,product_changes_list[0]['new_availability'])

    def test_if_other_fields_change_in_product_is_not_tracked_by_mistake(self):

        authenticate_user(self.api_client, self.admin_user)

        # create product
        response = create_or_update_product(self.api_client, self.sample_product_create_body).json()
        product_uid = response['uid']
        self.sample_product_create_body['name'] = "random name"
        # update product
        response = create_or_update_product(self.api_client, self.sample_product_create_body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # get changes list
        response = self.api_client.get("/product/price-change/list/", {'uid': product_uid}, format='json').json()
        product_changes_list = response['results']
        self.assertEqual(len(product_changes_list), 0)

    def test_if_price_change_list_view_returns_400_when_uid_parameter_is_not_set_in_body(self):
        # get changes list
        response = self.api_client.get("/product/price-change/list/", format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)