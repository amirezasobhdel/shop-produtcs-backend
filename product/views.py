from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from category.category_suggestion_service import get_suggested_category_based_on_product_name_and_features
from .exceptions import MissingParameterException
from .filters import ProductFilter, CustomOrderingFilter
from .models import Product
from .pagination import ProductListPagination, ProductPriceAndAvailabilityChangeListPagination
from .serializers import ProductWriteSerializer, ProductReadSerializer, ProductPriceAndAvailabilityChangeReadSerializer


@api_view(['POST'])
@permission_classes([IsAdminUser])
def create_or_update_product(request):
    serializer = ProductWriteSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    validated_data = serializer.validated_data

    defaults = dict(**validated_data)
    # get category_id from category suggestion service
    defaults['category_id'] = \
        get_suggested_category_based_on_product_name_and_features(defaults['name'], defaults['features'])

    product, created = Product.objects.update_or_create(
        page_url=validated_data['page_url'],
        defaults=defaults
    )

    return Response({'uid': product.uid}, status=status.HTTP_200_OK)


class ProductListView(ListAPIView):
    serializer_class = ProductReadSerializer
    # default sorting for products will be newest updated to oldest updated
    queryset = Product.objects.all().order_by('-updated')
    pagination_class = ProductListPagination
    filter_backends = [DjangoFilterBackend, CustomOrderingFilter]
    filterset_class = ProductFilter
    ordering_fields = ['price', 'updated']


class ProductPriceAndAvailabilityChangeListView(ListAPIView):
    serializer_class = ProductPriceAndAvailabilityChangeReadSerializer
    pagination_class = ProductPriceAndAvailabilityChangeListPagination

    def get_queryset(self):
        if 'uid' not in self.request.GET:
            raise MissingParameterException('uid')
        uid = self.request.GET['uid']
        return Product.objects.get(uid=uid).price_or_availability_changes.all().order_by('-created')


@api_view(['GET'])
def get_product_url(request):
    if 'uid' not in request.GET:
        raise MissingParameterException('uid')
    uid = request.GET['uid']
    product = get_object_or_404(Product, uid=uid)

    return Response({'uid': product.page_url}, status=status.HTTP_200_OK)