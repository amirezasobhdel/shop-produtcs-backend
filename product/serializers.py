from rest_framework import serializers

from product.models import Product, ProductPriceAndAvailabilityChange
from utils.string_formatter import add_tooman_string_to_price_number
from utils.time_formatter import get_time_ago_string


class ProductWriteSerializer(serializers.ModelSerializer):

    # page_url is a unique field in the Product model.
    # i use page_url as an identifier for when create_or_update_product view is called by crawlers
    # the default model serializer validator doesnt accept repetitive values for page_url because its unique
    # and it is in the body of the request. but to update products by crawlers, i had to write the below line of code.
    # todo : below line is bad practice, i should probably use serializers.Serializer.
    page_url = serializers.CharField()

    class Meta:
        model = Product
        fields = ["page_url",
                  "shop_domain",
                  "name",
                  "price",
                  "is_available",
                  "features"
                  ]


class ProductReadSerializer(serializers.ModelSerializer):

    price = serializers.SerializerMethodField()
    updated = serializers.SerializerMethodField()
    product_redirect_url = serializers.SerializerMethodField()
    product_price_list_url = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = ["uid",
                  "product_redirect_url",
                  "product_price_list_url",
                  "name",
                  "price",
                  "is_available",
                  "updated"
                  ]

    def get_price(self, product: Product):
        return add_tooman_string_to_price_number(product.price)

    def get_updated(self, product: Product):
        return get_time_ago_string(product.updated)

    def get_product_redirect_url(self, product: Product):
        return f"/product/redirect/?uid={product.uid}"

    def get_product_price_list_url(self, product: Product):
        return f"/product/price-change/list/?uid={product.uid}"


class ProductPriceAndAvailabilityChangeReadSerializer(serializers.ModelSerializer):

    old_price = serializers.SerializerMethodField()
    new_price = serializers.SerializerMethodField()
    price_change_time = serializers.SerializerMethodField()

    class Meta:
        model = ProductPriceAndAvailabilityChange
        fields = ["old_price",
                  "new_price",
                  "old_availability",
                  "new_availability",
                  "price_change_time"
                  ]

    def get_old_price(self, product_change: ProductPriceAndAvailabilityChange):
        return add_tooman_string_to_price_number(product_change.old_price)

    def get_new_price(self, product_change: ProductPriceAndAvailabilityChange):
        return add_tooman_string_to_price_number(product_change.new_price)

    def get_price_change_time(self, product_change: ProductPriceAndAvailabilityChange):
        return get_time_ago_string(product_change.created)
