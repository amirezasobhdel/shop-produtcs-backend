from django.contrib import admin
from django.db.models import Min, Max, Subquery

from product.models import Product, ProductPriceAndAvailabilityChange, ProductMinPriceChange


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass


@admin.register(ProductPriceAndAvailabilityChange)
class ProductPriceAndAvailabilityChangeAdmin(admin.ModelAdmin):
    pass


@admin.register(ProductMinPriceChange)
class ProductMinPriceChangeAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        each_products_min_price_change_ids = ProductMinPriceChange.objects.values('product_prk').annotate(max_id=Max('id'))
        return ProductMinPriceChange.objects.filter(id__in=Subquery(each_products_min_price_change_ids.values('max_id')))
