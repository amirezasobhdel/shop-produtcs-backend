import django_filters
from django_filters.rest_framework import FilterSet
from rest_framework.filters import OrderingFilter

from product.models import Product
from category.models import Category


class ProductFilter(FilterSet):

    # all the leaf sub categories should be found and then the products of those leaf sub categories should be filtered
    category_id = django_filters.NumberFilter(
        field_name='category_id', method='get_all_leaf_sub_categories_queryset', label="Category")

    def get_all_leaf_sub_categories_queryset(self, queryset, field_name, value):

        category = Category.objects.get(id=value)

        if category.is_leaf_node():
            return queryset.filter(category_id=value)
        else:
            return queryset.filter(category_id__in=category.get_leafnodes().values('id'))

    class Meta:
        model = Product
        fields = {
            'is_available': ['exact'],
            'price': ['lt', 'gt'],
            'category_id': ['exact']
        }


class CustomOrderingFilter(OrderingFilter):
    ordering_param = 'sort'
