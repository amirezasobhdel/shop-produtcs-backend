from rest_framework.exceptions import APIException


class MissingParameterException(APIException):
    status_code = 400
    detail = 'required parameter not provided!'
    code = 'param_not_provided'

    def __init__(self, parameter=None):
        if parameter:
            self.detail = {
                parameter: f'required {parameter} parameter not provided!'
            }
