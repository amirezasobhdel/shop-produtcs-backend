from rest_framework.pagination import PageNumberPagination


class ProductListPagination(PageNumberPagination):
    page_size = 10


class ProductPriceAndAvailabilityChangeListPagination(PageNumberPagination):
    page_size = 10
