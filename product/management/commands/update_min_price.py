from django.core.management.base import BaseCommand
from django.conf import settings

import requests.adapters

from product.models import ProductMinPriceChange

session = requests.Session()
adapter = requests.adapters.HTTPAdapter(pool_connections=100, pool_maxsize=100)
session.mount(settings.SUGGESTOIN_BASE_URL, adapter)


class Command(BaseCommand):
    help = 'Displays current time'

    def handle(self, *args, **kwargs):
        prks = [
            '3e7e8907-cd2a-4668-b8ea-8017eeed6f64',
            '24c7f526-9755-4344-9e95-7bad9fce88bd',
        ]

        for prk in prks:
            resp = session.get(f'https://api.torob.com/v4/base-product/details-log-click/?source=next&discover_method=search&_bt__experiment=&prk={prk}&rank=2&search_id=62f4d65337675a21f1138547&suid=62f4d65337675a21f1138547&max_seller_count=30&cities=')

            if resp.json()['products_info']['result'][0]['is_adv']:
                ind = 1
            else:
                ind = 0

            shop_name = resp.json()['products_info']['result'][ind]['shop_name']
            price = resp.json()['products_info']['result'][ind]['price']

            last_min_price = ProductMinPriceChange.objects.filter(product_prk=prk).order_by('-date').first()

            if last_min_price is None or last_min_price.price > price:
                p = ProductMinPriceChange()
                p.shop_name = shop_name
                p.product_prk = prk
                p.price = price
                p.save()
