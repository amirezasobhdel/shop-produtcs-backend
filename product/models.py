from django.db import models, transaction
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_uidfield import UIDField
from mptt.fields import TreeForeignKey
from tracking_model import TrackingModelMixin
from category.models import Category


class Product(models.Model, TrackingModelMixin):
    TRACKED_FIELDS = ["price", 'is_available']
    uid = UIDField(max_length=12, unique=True, db_index=True)
    page_url = models.URLField(unique=True, db_index=True)
    shop_domain = models.CharField(max_length=40)
    name = models.CharField(max_length=40)
    price = models.IntegerField()
    is_available = models.BooleanField()
    features = models.JSONField(default=dict)
    category = TreeForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True)
    updated = models.DateTimeField(auto_now=True)

    # an alternative can be a post_save signal, but they can be unpredictable sometimes
    # so i will just override the save method
    @transaction.atomic()
    def save(self, *args, **kwargs):
        super(Product, self).save(*args, **kwargs)
        # check if any of the product tracked fields are changed in the update
        changed_track_fields_dict = self.tracker.changed

        if len(changed_track_fields_dict) > 0:
            # there are changes in price or is_available, so we should save a new change item
            new_change = ProductPriceAndAvailabilityChange()
            new_change.product = self

            new_change.new_availability = self.is_available
            new_change.old_availability = self.is_available
            if "is_available" in changed_track_fields_dict:
                new_change.old_availability = changed_track_fields_dict["is_available"]

            new_change.new_price = self.price
            new_change.old_price = self.price
            if "price" in changed_track_fields_dict:
                new_change.old_price = changed_track_fields_dict["price"]

            new_change.save()


class ProductPriceAndAvailabilityChange(models.Model):
    old_price = models.IntegerField()
    new_price = models.IntegerField()
    old_availability = models.BooleanField()
    new_availability = models.BooleanField()
    created = models.DateTimeField(auto_now_add=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='price_or_availability_changes')


class ProductMinPriceChange(models.Model):
    product_prk = models.CharField(max_length=36)
    shop_name = models.CharField(max_length=20)
    price = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)
