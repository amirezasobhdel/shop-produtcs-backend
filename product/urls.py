from django.urls import path
import product.views as views

app_name = "product"
urlpatterns = [
    path("create-or-update/", views.create_or_update_product),
    path("list/", views.ProductListView.as_view()),
    path("price-change/list/", views.ProductPriceAndAvailabilityChangeListView.as_view()),
    path("redirect/", views.get_product_url),
]
