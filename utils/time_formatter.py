import timeago, datetime


def get_time_ago_string(date_time):
    now = datetime.datetime.now().astimezone()
    return timeago.format(date_time, now, 'fa_IR')
