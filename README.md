# Shop_Products_Backend

the project is basically the implementation of a couple functionalities about products in a crawler based shopping website.



## Functionalities

### Categories

each product should have a category. the categories data structure that I have implemented is in the form of Tree's. to explain more, each category can have a parent category, multiple categories can have a single parent category and so on.

**Note : ** only the leaf categories can have products.

**Note : ** the depth of each tree can not exceed to 4.

Let's look at the example below. 

![Categories](Categories.png)

only the galaxy, running, and sandals can be assigned to products as their category.

**I have used Django MPTT to efficiently implement this functionality in the database**



### Category Suggestion service

the crawler service can always add products to our service, when adding or updating a product, we have a separate AI service to call and get the suggested category based on the products features.

On production, I create a connection pool to send requests to the AI service, and when testing I mock the service.



### Product View

for the products list view, I have implemented different filters and ordering policies.



### Gitlab CI pipeline

I have written two stages build, and test.

In build stage, I build the image and then push it to gitlab container registry. and in the test, I run the tests.