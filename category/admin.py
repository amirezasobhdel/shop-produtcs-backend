from django.contrib import admin

from django.contrib import admin
from .models import Category
from mptt.admin import DraggableMPTTAdmin


@admin.register(Category)
class CategoryAdmin(DraggableMPTTAdmin):
    pass
