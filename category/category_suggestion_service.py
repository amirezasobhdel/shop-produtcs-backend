from django.conf import settings
from category.models import Category
import random
import requests.adapters

if not settings.MOCK_EXTERNAL_SERVICES:
    session = requests.Session()
    adapter = requests.adapters.HTTPAdapter(pool_connections=100, pool_maxsize=100)
    session.mount(settings.SUGGESTOIN_BASE_URL, adapter)


def get_suggested_category_based_on_product_name_and_features(product_name, product_features):
    if settings.MOCK_EXTERNAL_SERVICES:
        return mock_get_suggested_category_based_on_product_name_and_features(product_name, product_features)
    else:
        return real_get_suggested_category_based_on_product_name_and_features(product_name, product_features)


def mock_get_suggested_category_based_on_product_name_and_features(product_name, product_features):

    categories = Category.objects.all()
    # only the leaf categories can have products, so retrieve all leaf categories
    leaf_categories = [l for l in categories if l.is_leaf_node()]

    # randomly select a leaf category
    return random.choice(leaf_categories).id


def real_get_suggested_category_based_on_product_name_and_features(product_name, product_features):

    url = settings.SUGGESTOIN_BASE_URL

    resp = session.post(url + "product/suggest-category/",
                        data={
                            'name': product_name,
                            'features': product_features,
                        })

    suggested_category_id = int(resp.json()['id'])

    return suggested_category_id
