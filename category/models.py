from django.core.exceptions import ValidationError
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Category(MPTTModel):
    name = models.CharField(max_length=40)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    MAX_CATEGORY_TREE_DEPTH = 3

    def clean(self):
        # hierarchical categories can only be 3 levels deep, so these lines are for that :)
        parent = self.parent
        if parent and parent.get_level() + 1 >= self.MAX_CATEGORY_TREE_DEPTH:
            raise ValidationError(
                "Can't create 4th level category"
            )

    def save(self, *args, **kwargs):
        self.full_clean()
        return super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.name
