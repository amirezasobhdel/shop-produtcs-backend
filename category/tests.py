from django.core.exceptions import ValidationError
from django.test import TestCase

from category.models import Category


class CategoryTestCase(TestCase):

    def setUp(self):
        pass

    def test_if_creating_4th_level_category_raises_validation_error(self):
        level_1_category = Category.objects.create(name='level_1')
        level_2_category = Category.objects.create(name='level_2', parent=level_1_category)
        level_3_category = Category.objects.create(name='level_3', parent=level_2_category)
        # 4th level creation as callable
        self.assertRaises\
            (ValidationError, Category.objects.create, name='level_4', parent=level_3_category)
