from rest_framework.generics import ListAPIView

from category.models import Category
from category.pagination import CategoryListPagination
from category.serializers import CategorySerializer


class CategoryListView(ListAPIView):
    serializer_class = CategorySerializer
    pagination_class = CategoryListPagination
    queryset = Category.objects.all()

