from django.urls import path
import category.views as views

app_name = "category"
urlpatterns = [
    path("list/", views.CategoryListView.as_view()),
]
